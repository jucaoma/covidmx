# Instrucciones de uso de instalación

## Requerimientos

- Pdftotext

### Opcionalmente

- R
- RStudio

## Uso

Una vez descargado el script otorgarle permisos de ejecución (chmod +x).

Para su uso se necesita de dos parámetros, el primero es el día de la fecha y el segundo es la URL de donde se descargara el PDF

**Ejemplo**

./covidmx.sh 22 "https://www.gob.mx/cms/uploads/attachment/file/542707/Tabla_casos_positivos_COVID-19_resultado_InDRE_2020.03.22.pdf"

Lo cual dará como resultado que en las carpetas preseleccionadas se guarden con la fecha de ejecución la base diaria, que contendrá el contenido completo del PDF, la base diaria, que contendrá el resumen por estados de los casos positivos, estos últimos se añadirán a la base general que contiene toda la información (del acumulado por estado) y un archivo con extensión rmd el cual puede sera abierto en Rstudio automáticamente, se tendrá que ejecutar dicho archivo desde la interfaz (Knit) lo cual creara un archivo html, que al cerrar el programa de RStudio sera por ultimo modificado con una ultima orden de sed para la presentación final que se muestra en la pagina actualmente.
