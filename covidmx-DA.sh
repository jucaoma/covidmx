#!/bin/bash
# Segunda versión del scrip para la generación de las graficas y los mapas de México apartir de los datos proporcionados por el gobierno de México en su plataforma para datos abiertos.
FECHA=`date +%d.%m.%Y`
DIA=`date +%d`
# Descarga de la base de datos del día
wget "http://datosabiertos.salud.gob.mx/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip" -O $FECHA.zip
unzip $FECHA.zip
# Ahora se crear el archivo rmd para el mapa y graficas de pacientes
echo "---">>Coronavirus.rmd
echo "Proyecto: \"Mapa dinámico de México con los casos confirmados(activos y no activos) de COVID-19\"">>Coronavirus.rmd
echo "licencia: \"GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt\"">>Coronavirus.rmd
echo "Autor: \"ZoRrO\"">>Coronavirus.rmd
echo "Fecha de Creación: \"15/3/2020\"">>Coronavirus.rmd
echo "output: html_document">>Coronavirus.rmd
echo "editor_options: ">>Coronavirus.rmd
echo "  chunk_output_type: inline">>Coronavirus.rmd
echo "---">>Coronavirus.rmd
echo " ">>Coronavirus.rmd
echo "\`\`\`{r setup, include=FALSE}">>Coronavirus.rmd
echo "knitr::opts_chunk\$set(echo = FALSE)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "<div style=\"text-align: center\">">>Coronavirus.rmd
echo "## Acumulativo de Casos Confirmados de SARS-Cov-2 en México\"">>Coronavirus.rmd
echo "### Total para el día "$FECHA": ">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "##### Librerias ####">>Coronavirus.rmd
echo "library(tidyverse)">>Coronavirus.rmd
echo "library(highcharter)">>Coronavirus.rmd
echo "# Recursos/Configuraciones Básicas">>Coronavirus.rmd
echo "# Directorio de trabajo ####">>Coronavirus.rmd
echo "setwd(\"/home/zorro/Documentos/Otros/COVID-19/covidmx\")">>Coronavirus.rmd
echo "BD <- \"2008"$DIA"COVID19MEXICO.csv\"">>Coronavirus.rmd
echo "# Base de datos ####">>Coronavirus.rmd
echo "info <- data.frame(read.csv(BD,header = T))">>Coronavirus.rmd
echo "# Y seleccionar solo casos positivos ####">>Coronavirus.rmd
echo "info <- subset(info,RESULTADO==1)">>Coronavirus.rmd
echo "print(nrow(info))">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "estados <- data.frame(read.csv(\"dic/Estados.csv\",header = T))">>Coronavirus.rmd
echo "municipios<-data.frame(read.csv(\"dic/Municipios.csv\",header = T))">>Coronavirus.rmd
echo "resultados <- data.frame(read.csv(\"dic/resultado.csv\",header = T))">>Coronavirus.rmd
echo "sexo <- data.frame(read.csv(\"dic/sexo.csv\", header = T))">>Coronavirus.rmd
echo "# Crear base de datos">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "# Convertir los nombres de los estados y municipios segun su clave####">>Coronavirus.rmd
echo "est.muni <- info %>% select(c(\"ENTIDAD_RES\",\"MUNICIPIO_RES\"))">>Coronavirus.rmd
echo "estados <- estados %>% select(c(\"Clave.De.AGEE\",\"Nombre.De.AGEE\"))">>Coronavirus.rmd
echo "municipios <- municipios %>% select(c(\"Cve_Ent\",\"Cve_Mun\",\"Nom_Mun\"))">>Coronavirus.rmd
echo "info2 <- data.frame(Estado=\"Prueba\",">>Coronavirus.rmd
echo "                    Municipio=\"Prueba\")">>Coronavirus.rmd
echo "i = 0">>Coronavirus.rmd
echo "while (i != 33) {">>Coronavirus.rmd
echo "  temp <- subset(est.muni, ENTIDAD_RES==i)">>Coronavirus.rmd
echo "  temp2 <- subset(municipios, Cve_Ent==i)">>Coronavirus.rmd
echo "  temp2 <- temp2 %>% select(-c(\"Cve_Ent\"))">>Coronavirus.rmd
echo "  temp <- merge(temp, estados,">>Coronavirus.rmd
echo "                by.x = \"ENTIDAD_RES\", by.y = \"Clave.De.AGEE\",">>Coronavirus.rmd
echo "                all.x=TRUE, all.y=FALSE)">>Coronavirus.rmd
echo "  temp <- merge(temp, temp2,">>Coronavirus.rmd
echo "                by.x = \"MUNICIPIO_RES\", by.y = \"Cve_Mun\",">>Coronavirus.rmd
echo "                all.x=TRUE, all.y=FALSE)">>Coronavirus.rmd
echo "  temp <- temp %>% select(c(\"Nombre.De.AGEE\",\"Nom_Mun\"))">>Coronavirus.rmd
echo "  colnames(temp) <- c(\"Estado\",\"Municipio\")">>Coronavirus.rmd
echo "  info2 <- rbind(info2,temp)">>Coronavirus.rmd
echo "  i<-i+1">>Coronavirus.rmd
echo "}">>Coronavirus.rmd
echo "info2 <- info2[-c(1),]">>Coronavirus.rmd
echo "rm(temp,temp2,municipios,est.muni,resultados,i)">>Coronavirus.rmd
echo "info2[,1] <- as.character(info2[,1])">>Coronavirus.rmd
echo "info2[,2] <- as.character(info2[,2])">>Coronavirus.rmd
echo "# Base por Sexo ####">>Coronavirus.rmd
echo "SEXO <- info %>% select(SEXO)">>Coronavirus.rmd
echo "SEXO <- merge(SEXO, sexo,">>Coronavirus.rmd
echo "               by.x = \"SEXO\", by.y = \"CLAVE\",">>Coronavirus.rmd
echo "               all.x=TRUE, all.y=FALSE)">>Coronavirus.rmd
echo "SEXO <- SEXO %>% group_by(DESCRIPCIÓN) %>% tally()">>Coronavirus.rmd
echo "rm(sexo)">>Coronavirus.rmd
echo "# Base por estados ####">>Coronavirus.rmd
echo "ESTADOS <- info2 %>% group_by(Estado) %>% tally()">>Coronavirus.rmd
echo "ESTADOS[16,1] <- \"Michoacán\"">>Coronavirus.rmd
echo "ESTADOS[30,1]<- \"Veracruz\"">>Coronavirus.rmd
echo "ESTADOS[8,1]<- \"Coahuila\"">>Coronavirus.rmd
echo "ESTADOS[7,1]<- \"Distrito Federal\"">>Coronavirus.rmd
echo "rm(estados)">>Coronavirus.rmd
echo "# Grafica de México ####">>Coronavirus.rmd
echo "hcmap(\"countries/mx/mx-all\",data = ESTADOS, value = \"n\",">>Coronavirus.rmd
echo "      joinBy = c(\"name\", \"Estado\"), name = \"Confirmados\",">>Coronavirus.rmd
echo "      dataLabels = list(enabled = TRUE),">>Coronavirus.rmd
echo "      borderColor = \"#0EAD82\", borderWidth = 0.1) %>%">>Coronavirus.rmd
echo "  hc_subtitle(text =\"Fuente: Gobierno de México\")%>%">>Coronavirus.rmd
echo "  hc_colorAxis(minColor = \"#F57779\", maxColor = \"#360001\")%>%">>Coronavirus.rmd
echo "  hc_mapNavigation(enabled = T) %>%">>Coronavirus.rmd
echo "  hc_add_theme(hc_theme_darkunica())">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "<div style=\"text-align: right\">\*Estados en blanco, sin casos confirmados*</div>">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "## Graficas por estado.">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo " |Estados| | |">>Coronavirus.rmd
echo "-------|-|-|-|">>Coronavirus.rmd
echo "[AGUASCALIENTES](estados/1.html)     |[GUANAJUATO](estados/11.html)|[PUEBLA](estados/21.html)|[YUCATÁN](estados/31.html)">>Coronavirus.rmd
echo "[BAJA CALIFORNIA](estados/2.html)    |[GUERRERO](estados/12.html)|[QUERÉTARO](estados/22.html)|[ZACATECAS](estados/32.html)">>Coronavirus.rmd
echo "[BAJA CALIFORNIA SUR](estados/3.html)|[HIDALGO](estados/13.html)|[QUINTANA ROO](estados/23.html)|">>Coronavirus.rmd
echo "[CAMPECHE](estados/4.html)           |[JALISCO](estados/14.html)|[SAN LUIS POTOSÍ](estados/24.html)|">>Coronavirus.rmd
echo "[COAHUILA](estados/5.html)           |[MÉXICO](estados/15.html)|[SINALOA](estados/25.html)|">>Coronavirus.rmd
echo "[COLIMA](estados/6.html)             |[MICHOACÁN](estados/16.html)|[SONORA](estados/26.html)|">>Coronavirus.rmd
echo "[CHIAPAS](estados/7.html)            |[MORELOS](estados/17.html)|[TABASCO](estados/27.html)|">>Coronavirus.rmd
echo "[CHIHUAHUA](estados/8.html)          |[NAYARIT](estados/18.html)|[TAMAULIPAS](estados/28.html)|">>Coronavirus.rmd
echo "[CIUDAD DE MÉXICO](estados/9.html)   |[NUEVO LEÓN](estados/19.html)|[TLAXCALA](estados/29.html)|">>Coronavirus.rmd
echo "[DURANGO](estados/10.html)            |[OAXACA](estados/20.html)|[VERACRUZ](estados/30.html)|">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica por sexo">>Coronavirus.rmd
echo " ">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=F}">>Coronavirus.rmd
echo "hchart(SEXO , \"pie\", hcaes(x = DESCRIPCIÓN, y = n)) %>%">>Coronavirus.rmd
echo "  hc_add_theme(hc_theme_538())">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica por grupo etario">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=F}">>Coronavirus.rmd
echo "grupo.etario <- data.frame(\"Grupo\"=c(\"<1\",\"1-4\",\"5-9\",\"10-14\",\"15-19\",">>Coronavirus.rmd
echo "                                     \"20-24\",\"25-30\",\"31-35\",\"36-40\",">>Coronavirus.rmd
echo "                                      \"41-44\",\"45-49\",\"50-54\",">>Coronavirus.rmd
echo "                                     \"55-59\",\"60-64\",\"65-74\",\"75-84\",">>Coronavirus.rmd
echo "                                     \"85-90\",\">90\"), \"n\"=0)">>Coronavirus.rmd
echo "grupo.etario[1,2] <- info %>% select(EDAD) %>% filter(EDAD<1) %>% count()">>Coronavirus.rmd
echo "grupo.etario[2,2] <- info %>% select(EDAD) %>% filter(EDAD>0 & EDAD<5) %>% count()">>Coronavirus.rmd
echo "grupo.etario[3,2] <- info %>% select(EDAD) %>% filter(EDAD>4 & EDAD<10) %>% count() # entre 5 y 9 EDAD">>Coronavirus.rmd
echo "grupo.etario[4,2] <- info %>% select(EDAD) %>% filter(EDAD>9 & EDAD<15) %>% count()">>Coronavirus.rmd
echo "grupo.etario[5,2] <- info %>% select(EDAD) %>% filter(EDAD>14 & EDAD<20) %>% count()">>Coronavirus.rmd
echo "grupo.etario[6,2] <- info %>% select(EDAD) %>% filter(EDAD>19 & EDAD<25) %>% count()">>Coronavirus.rmd
echo "grupo.etario[7,2] <- info %>% select(EDAD) %>% filter(EDAD>24 & EDAD<31) %>% count()">>Coronavirus.rmd
echo "grupo.etario[8,2] <- info %>% select(EDAD) %>% filter(EDAD>30 & EDAD<36) %>% count()">>Coronavirus.rmd
echo "grupo.etario[9,2] <- info %>% select(EDAD) %>% filter(EDAD>35 & EDAD<41) %>% count()">>Coronavirus.rmd
echo "grupo.etario[10,2] <- info %>% select(EDAD) %>% filter(EDAD>40 & EDAD<45) %>% count()">>Coronavirus.rmd
echo "grupo.etario[11,2] <- info %>% select(EDAD) %>% filter(EDAD>44 & EDAD<50) %>% count()">>Coronavirus.rmd
echo "grupo.etario[12,2] <- info %>% select(EDAD) %>% filter(EDAD>49 & EDAD<55) %>% count()">>Coronavirus.rmd
echo "grupo.etario[13,2] <- info %>% select(EDAD) %>% filter(EDAD>54 & EDAD<60) %>% count()">>Coronavirus.rmd
echo "grupo.etario[14,2] <- info %>% select(EDAD) %>% filter(EDAD>59 & EDAD<65) %>% count()">>Coronavirus.rmd
echo "grupo.etario[15,2] <- info %>% select(EDAD) %>% filter(EDAD>64 & EDAD<75) %>% count()">>Coronavirus.rmd
echo "grupo.etario[16,2] <- info %>% select(EDAD) %>% filter(EDAD>74 & EDAD<85) %>% count()">>Coronavirus.rmd
echo "grupo.etario[17,2] <- info %>% select(EDAD) %>% filter(EDAD>84 & EDAD<91) %>% count()">>Coronavirus.rmd
echo "grupo.etario[18,2] <- info %>% select(EDAD) %>% filter(EDAD>90) %>% count()">>Coronavirus.rmd
echo "###">>Coronavirus.rmd
echo "grupo.etario\$Grupo = factor(grupo.etario\$Grupo, levels = c(\"<1\",">>Coronavirus.rmd
echo "                                                           \"1-4\",\"5-9\",">>Coronavirus.rmd
echo "                                                           \"10-14\",">>Coronavirus.rmd
echo "                                                           \"15-19\",\"20-24\",">>Coronavirus.rmd
echo "                                                           \"25-30\",\"31-35\",\"36-40\",">>Coronavirus.rmd
echo "                                                           \"41-44\",\"45-49\",">>Coronavirus.rmd
echo "                                                           \"50-54\",\"55-59\",">>Coronavirus.rmd
echo "                                                           \"60-64\",\"65-74\",">>Coronavirus.rmd
echo "                                                           \"75-84\",\"85-90\",">>Coronavirus.rmd
echo "                                                           \">90\"))">>Coronavirus.rmd
echo "hchart(grupo.etario, \"column\", hcaes(x = Grupo, y = n)) %>%">>Coronavirus.rmd
echo "  hc_add_theme(hc_theme_538())">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica por tipo de paciente">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "rm(ESTADOS,info2,SEXO)">>Coronavirus.rmd
echo "descrip <- function(bd){">>Coronavirus.rmd
echo "  tpo.p <- bd %>% group_by(TIPO_PACIENTE) %>% tally()">>Coronavirus.rmd
echo "  TPO <- data.frame(read.csv(\"dic/TIPO_PACIENTE.csv\"))">>Coronavirus.rmd
echo "  tpo.p <- merge(tpo.p, TPO,">>Coronavirus.rmd
echo "                 by.x = \"TIPO_PACIENTE\", by.y = \"TIPO_PACIENTE\",">>Coronavirus.rmd
echo "                 all.x = TRUE, all.y = FALSE)">>Coronavirus.rmd
echo "  tpo.p <- tpo.p %>% select(-c(\"TIPO_PACIENTE\"))">>Coronavirus.rmd
echo "  rm(TPO)">>Coronavirus.rmd
echo "  colnames(tpo.p) <- c(\"n\",\"TIPO_PACIENTE\")">>Coronavirus.rmd
echo "  # Hospitalizados + Neumonia">>Coronavirus.rmd
echo "  hospi.N <- bd %>% select(c(\"TIPO_PACIENTE\",\"NEUMONIA\"))">>Coronavirus.rmd
echo "  hospi.N <- subset(hospi.N,TIPO_PACIENTE==2)">>Coronavirus.rmd
echo "  hospi.N <- subset(hospi.N,NEUMONIA==1)">>Coronavirus.rmd
echo "  temp <- data.frame(n=nrow(hospi.N),">>Coronavirus.rmd
echo "                     TIPO_PACIENTE=\"Hospitalizado + Neumonia\")">>Coronavirus.rmd
echo "  tpo.p <- rbind(tpo.p,temp)">>Coronavirus.rmd
echo "  rm(temp,hospi.N)">>Coronavirus.rmd
echo "  # Hospi + Neumonia + intubado">>Coronavirus.rmd
echo "  hospi.NI <- bd %>% select(c(\"TIPO_PACIENTE\",\"NEUMONIA\",\"INTUBADO\"))">>Coronavirus.rmd
echo "  hospi.NI <- subset(hospi.NI,TIPO_PACIENTE==2)">>Coronavirus.rmd
echo "  hospi.NI <- subset(hospi.NI,NEUMONIA==1)">>Coronavirus.rmd
echo "  hospi.NI <- subset(hospi.NI,INTUBADO==1)">>Coronavirus.rmd
echo "  temp <- data.frame(n=nrow(hospi.NI),">>Coronavirus.rmd
echo "                     TIPO_PACIENTE=\"Hospitalizado + Neumonia + Intubado\")">>Coronavirus.rmd
echo "  tpo.p <- rbind(tpo.p,temp)">>Coronavirus.rmd
echo "  rm(temp,hospi.NI)">>Coronavirus.rmd
echo "  # UCI">>Coronavirus.rmd
echo "  uci<- bd %>% group_by(UCI) %>% tally()">>Coronavirus.rmd
echo "  temp <- data.frame(n=uci[1,2],">>Coronavirus.rmd
echo "                     TIPO_PACIENTE=\"UCI\")">>Coronavirus.rmd
echo "  tpo.p <- rbind(tpo.p,temp)">>Coronavirus.rmd
echo "  rm(temp,uci)">>Coronavirus.rmd
echo "  # Hospi + Neumonia + intubado + UCI">>Coronavirus.rmd
echo "  hospi.NIU <- bd %>% select(c(\"TIPO_PACIENTE\",\"NEUMONIA\",\"INTUBADO\",\"UCI\"))">>Coronavirus.rmd
echo "  hospi.NIU <- subset(hospi.NIU,TIPO_PACIENTE==2)">>Coronavirus.rmd
echo "  hospi.NIU <- subset(hospi.NIU,NEUMONIA==1)">>Coronavirus.rmd
echo "  hospi.NIU <- subset(hospi.NIU,INTUBADO==1)">>Coronavirus.rmd
echo "  hospi.NIU <- subset(hospi.NIU,UCI==1)">>Coronavirus.rmd
echo "  temp <- data.frame(n=nrow(hospi.NIU),">>Coronavirus.rmd
echo "                     TIPO_PACIENTE=\"Hospitalizado + Neumonia + Intubado + UCI\")">>Coronavirus.rmd
echo "  tpo.p <- rbind(tpo.p,temp)">>Coronavirus.rmd
echo "  rm(hospi.NIU)">>Coronavirus.rmd
echo "  hchart(tpo.p,\"column\", hcaes(x=TIPO_PACIENTE,y=n))%>%">>Coronavirus.rmd
echo "    hc_add_theme(hc_theme_538())">>Coronavirus.rmd
echo "}">>Coronavirus.rmd
echo "## General">>Coronavirus.rmd
echo "descrip(info)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes Diabeticos">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## DM">>Coronavirus.rmd
echo "DM <- subset(info,DIABETES==1)">>Coronavirus.rmd
echo "descrip(DM)">>Coronavirus.rmd
echo "rm(DM)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes con Hipertención Arterial Sistemica">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## HAS">>Coronavirus.rmd
echo "HAS <- subset(info,HIPERTENSION==1)">>Coronavirus.rmd
echo "descrip(HAS)">>Coronavirus.rmd
echo "rm(HAS)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes con Obesidad">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "## Obecidad">>Coronavirus.rmd
echo "Ob <- subset(info,OBESIDAD == 1)">>Coronavirus.rmd
echo "descrip(Ob)">>Coronavirus.rmd
echo "rm(Ob)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes con patologías cardiovasculares">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## Cardiovascular">>Coronavirus.rmd
echo "cardio <- subset(info, CARDIOVASCULAR==1)">>Coronavirus.rmd
echo "descrip(cardio)">>Coronavirus.rmd
echo "rm(cardio)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes inmunosuprimidos">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## Inmunosuprimidos">>Coronavirus.rmd
echo "inmuno <- subset(info, INMUSUPR==1)">>Coronavirus.rmd
echo "descrip(inmuno)">>Coronavirus.rmd
echo "rm(inmuno)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes fumadores">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## Tabaquismo">>Coronavirus.rmd
echo "tab  <- subset(info,TABAQUISMO==1)">>Coronavirus.rmd
echo "descrip(tab)">>Coronavirus.rmd
echo "rm(tab)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes con falla renal">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## Renal">>Coronavirus.rmd
echo "rn <- subset(info,RENAL_CRONICA==1)">>Coronavirus.rmd
echo "descrip(rn)">>Coronavirus.rmd
echo "rm(rn)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "### Gráfica del estado de pacientes con EPOC">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>Coronavirus.rmd
echo "## EPOC">>Coronavirus.rmd
echo "epoc <- subset(info, EPOC==1)">>Coronavirus.rmd
echo "descrip(epoc)">>Coronavirus.rmd
echo "rm(epoc)">>Coronavirus.rmd
echo "\`\`\`">>Coronavirus.rmd
echo "">>Coronavirus.rmd
echo "</div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"><b><a href=\"https://datos.gob.mx/busca/dataset/informacion-referente-a-casos-covid-19-en-mexico\">Fuente General</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"><b><a href=\"https://gitlab.com/jucaoma/covidmx\">Nuestro GitLab(Archivos CSV, código fuente)</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Procesado con R v4.0.0 y RStudio v1.2.1335 </b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Autor: <a href=\"https://t.me/ZoRrO08\">ZoRrO</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/Ninjaburger\">Ninjaburger</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaboradora: <a href=\"https://t.me/Katjins\">Katjins</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/jucaoma\">Jucaoma</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/LaDataNo\">LaDataNo</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/petrohs\">petrohs</a></b></div>">>Coronavirus.rmd
echo " <div style=\"text-align: right\"><b>Licencia: <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC-BY-SA</a></b></div>">>Coronavirus.rmd
rstudio Coronavirus.rmd
nano Coronavirus.html
mv Coronavirus.html index.html
rm Coronavirus.rmd
