# covidx

## **TRABAJO EN PROGRESO, SEGÚN CAMBIE LA SITUACIÓN DE LA PANDEMIA**
## **Actualización 09/05/202 **

Se agrega el archivo covidmx-DA.sh e INSTRUCCIONES-DA los cuales corresponde al nuevo scrip e instrucciones de acuerdo a los datos proporcionados por el gobierno de México, ahora al otorgar la información en formato CSV no hay necesidad de continuar usando el scrip anterior que extraía la información de los PDF para luego convertirla a una base de datos CSV, el scrip es mas sencillo y con la facilidad de realizar una análisis de mayor profundidad, en la nueva carpeta ZIP, se están almacenando las bases de datos diarias que el gobierno proporciona para su futuro uso de ser necesario.

## Introducción

Este proyecto se realizo con la intención de resguardar y ofrecer de manera sencilla la información proporcionada por el [gobierno de México](https://www.gob.mx) durante esta pandemia.

El gobierno hasta el momento diariamente reporta los casos de COVID-19 en formato PDF en tres archivos, los cuales no son prácticos para su análisis posteriores, y con la intención de que esto se logre en un futuro hemos desarrollado un [script](https://es.wikipedia.org/wiki/Script) en [bash](https://es.wikipedia.org/wiki/Bash) que limpia y crea un nuevo archivo [csv](https://es.wikipedia.org/wiki/Valores_separados_por_comas) utilizando la librería de [Pdftotext](https://en.wikipedia.org/wiki/Pdftotext) y [sed](https://es.wikipedia.org/wiki/Sed_(inform%C3%A1tica)).

El script presentado aquí además de crear las bases de los reportes diarios, creara una base general donde se irán guardando todos los concentrados por estados, esa base general contendrá menos información que las bases diarias, así que si se desea profundizar en la naturaleza epidemiológica del evento se sugiere usar las bases limpias diarias (el resultado del procesamiento con pdftotext y sed).

Por desgracia el proyecto empezó el día 14/03/2020, varios días posteriores del primer reporte (28/02/2020) por lo que muchos PDF no se encontraron para ese momento, gracias a la ayuda de [Katjins](https://t.me/Katjins) quien realizo la búsqueda de los datos por estados previos a la fecha de inicio, es que se logro completar la linea de tiempo, sin embargo no se cuenta con las bases diarias de esos días, de lograr conseguir los PDF de esos días se podrá complementar y comparar los resultados finales.

Por ultimo, el script se desarrollo con el fin también de lograr realizar un reporte diario de un [mapa coroplético](https://es.wikipedia.org/wiki/Mapa_coropl%C3%A9tico) el cual muestre por estados la cantidad de casos positivos de COVID-19, sabiendo las limitaciones de este tipo de mapa al solo mostrar los datos acumulativos, lo cual puede llegar a crear mal interpretaciones del desarrollo natural de la pandemia, además de mostrar una linea de tiempo de los casos acumulativos por estados, todo esto utilizando el lenguaje de programación de [R](https://es.wikipedia.org/wiki/R_(lenguaje_de_programaci%C3%B3n)) con su IDE [Rstudio](https://es.wikipedia.org/wiki/RStudio).
Procesado con R v3.6.3 y RStudio v1.2.1335

Para conocer el uso del script favor de leer la sección de INSTRUCCIONES

## Agradecimientos

- Autor: [ZoRrO](https://t.me/ZoRrO08)
- Colaborador: [Ninjaburger](https://t.me/Ninjaburger)
- Colaboradora: [Katjins](https://t.me/Katjins)
- Colaborador: [Jucaoma](https://t.me/jucaoma)
- Colaborador: [LaDataNo](https://t.me/LaDataNo)

## Licencias
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

