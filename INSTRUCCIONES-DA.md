# Instrucciones de uso de instalación

## Requerimientos

- R, RStudio

## Uso

Una vez descargado el script otorgarle permisos de ejecución (chmod +x).

Para su uso, desde una terminal ejecutarlo "./covidmx-DA.sh

Lo cual dará como resultado que se descargue y renombre según la fecha la nueva base de datos, además de generar el archivo RMD necesario y ejecutable desde el RSTUDIO, con el cual se obtendrá el mapa general de México y las gráficas por pacientes.
