#!/bin/bash
# En este primer apartado el script descargar el PDF de la URL establecida como segundo parámetro, posterior a eso moverá y renombrara el PDF a la carpeta seleccionada, la cual debe ser previamente creada.
wget $2
mv *.pdf PDF/Abril/$1.04.2020.pdf
# En este segundo apartado se establece el parámetro de la fecha y se utiliza la librería pdftotext para extraer el texto del PDF y con sed lograr el formato deseado estilo CSV y que sera guardada en un archivo llamado FECHA+base.limpia.
# Es importante recalcar que este comando es la base de este trabajo y que fue escrito por Ninjaburger, con este comando es posible extraer de todos los PDF dados por el gobierno para esta pandemia y guardarlo en una base de datos en bruto CSV.
FECHA=`date +%d.%m.%Y`
pdftotext -layout -nopgbrk PDF/Abril/$1.04.2020.pdf - \
| sed -E 's/([[:alnum:]]+)\s*,\s*([[:alnum:]]+)/\1 - \2/gI' \
| sed -e 's/^[ tab]*//' | sed 's/[[:space:]]\{2,\}/,/g' \
| sed -e 's/[[:space:]]\(confirmado\)/,\1/gI' \
| sed --regexp-extended   '/./{H;$!d} ; x ; s/(Estados)\n([0-9]+)(,[^,]+,[^,]+,[^,]+,[^,]+,[^,]+,)/\2\3\1 /g' \
| sed --regexp-extended   '/./{H;$!d} ; x ; s/(Estados)\n([^\n0-9]+)\n([0-9]+)(,[^,]+,[^,]+,[^,]+,[^,]+,)/ \3,\2\4\1 /g' \
| sed -e 's/ciudad de m[eé]xico/Distrito Federal/gI' \
| sed -E 's/([A-Z])([A-Z]+)/\1\L\2/g' \
| sed 's/^\s*//g' \
| sed --regexp-extended 's/^(Baja California),?([0-9]+)/\2,\1/g' \
| sed --regexp-extended '/./{H;$!d} ; x ; s/\b(Baja California)\n([0-9]+)/\2,\1/g' \
| sed 's/Baja California,Sur/Baja California Sur/g' \
| sed '/^[^0-9]/d' \
| sed '/^$/d' \
| sed "s/$/,$FECHA/g"  \
| sed 's/Queretaro/Querétaro/g' \
| sed '1i NCasos,Estado,Sexo,Edad,F.I.S,PCR,Procedencia,F.a.M,Fecha' > CSV/Abril/$1.04.2020.base.limpia.csv
# A partir de este punto se creara una script para R, el cual procesara la base creada previamente con sed y creara una nueva con la suma total por estados y casos positivos.
echo "#!/usr/bin/env Rscript" > limpiar.base.r
echo "library(tidyverse)" >> limpiar.base.r
echo "data<- read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/Abril/"$1".04.2020.base.limpia.csv\",header = TRUE)" >> limpiar.base.r
echo "Casos <- data.frame(data %>% group_by(Estado)%>% tally())" >> limpiar.base.r
echo "write.csv(Casos,file = \"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/Abril/"$1".04.2020.base.diaria.csv\")" >> limpiar.base.r
Rscript limpiar.base.r
# Aqui con sed y cat se junta la base por estados en una lista general que contiene el nombre del estado, los casos reportados en el día y la fecha como tal.
cat CSV/Abril/$1.04.2020.base.diaria.csv | sed "s/$/,$FECHA/g" | sed '1i,Estado,n,Fecha' > .tmp1.csv
sed '1,2d' .tmp1.csv > .tmp2.csv
cat .tmp2.csv >> CSV/general.csv
# Y a partir de este punto se creara el archivo con extensión rmd para ser ejecutado con RStudio para crear la linea de tiempo y el mapa.
echo "---" > CoronaVirus.rmd
echo "Proyecto: \"Mapa dinámico de México con los casos confirmados(activos y no activos) de COVID-19\"">>CoronaVirus.rmd
echo "licencia: \"GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt\"">>CoronaVirus.rmd
echo "Autor: \"ZoRrO\"">>CoronaVirus.rmd
echo "Fecha de Creación: \"15/3/2020\"">>CoronaVirus.rmd
echo "output: html_document">>CoronaVirus.rmd
echo "---">>CoronaVirus.rmd
echo " ">>CoronaVirus.rmd
echo "\`\`\`{r setup, include=FALSE}">>CoronaVirus.rmd
echo "knitr::opts_chunk\$set(echo = TRUE)">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "<center>">>CoronaVirus.rmd
echo "## Acumulativo de Casos Confirmados de COVID-19 en México">>CoronaVirus.rmd
echo "### Total para el día "$1"/03/2020: ">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>CoronaVirus.rmd
echo "library(tidyverse)">>CoronaVirus.rmd
echo "Casos<- data.frame(read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/Abril/"$1".04.2020.base.diaria.csv\"))">>CoronaVirus.rmd
echo "a<- \" Casos\"">>CoronaVirus.rmd
echo "paste(as.character(sum(Casos\$n)),a)">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "### Total de confirmados para los últimos 14 días:">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=F}">>CoronaVirus.rmd
echo "library(tidyverse)">>CoronaVirus.rmd
echo "info <- data.frame(read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/general.csv\"))">>CoronaVirus.rmd
echo "info <- info %>% select(-c(X))">>CoronaVirus.rmd
echo "info\$Fecha <- as.Date(info\$Fecha, format=\"%d.%m.%Y\")">>CoronaVirus.rmd
echo "fecha.A <- info[nrow(info),3]">>CoronaVirus.rmd
echo "fecha.14 <- fecha.A-14">>CoronaVirus.rmd
echo "total.A <- subset(info, Fecha == fecha.A)">>CoronaVirus.rmd
echo "Total.A <- as.numeric(sum(total.A\$n))">>CoronaVirus.rmd
echo "total.14 <- subset(info, Fecha == fecha.14)">>CoronaVirus.rmd
echo "Total.14 <- as.numeric(sum(total.14\$n))">>CoronaVirus.rmd
echo "Casos.14 <- Total.A - Total.14">>CoronaVirus.rmd
echo "a <- \"Casos\"">>CoronaVirus.rmd
echo "paste(as.character(Casos.14),a)">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "</center>">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=FALSE}">>CoronaVirus.rmd
echo "fuente <- \"Fuente "$2"\"">>CoronaVirus.rmd
echo "library(highcharter)">>CoronaVirus.rmd
echo "library(xts)">>CoronaVirus.rmd
echo "library(dygraphs)">>CoronaVirus.rmd
echo " Casos <- Casos %>% select(-c(\"X\"))">>CoronaVirus.rmd
echo " Casos <- Casos[order(Casos\$n),]">>CoronaVirus.rmd
echo " hcmap(\"countries/mx/mx-all\",data = Casos, value = \"n\",">>CoronaVirus.rmd
echo "       joinBy = c(\"name\", \"Estado\"), name = \"Confirmados\",">>CoronaVirus.rmd
echo "       dataLabels = list(enabled = TRUE),">>CoronaVirus.rmd
echo "       borderColor = \"#0EAD82\", borderWidth = 0.1) %>%">>CoronaVirus.rmd
echo "  hc_subtitle(text =fuente)%>%">>CoronaVirus.rmd
echo "  hc_colorAxis(minColor = \"#F57779\", maxColor = \"#360001\")%>%">>CoronaVirus.rmd
echo "  hc_mapNavigation(enabled = T) %>%">>CoronaVirus.rmd
echo "  hc_add_theme(hc_theme_darkunica())">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "<div style=\"text-align: right\">\\*Estados en blanco, sin casos confirmados*</div>">>CoronaVirus.rmd
echo "<div style=\"text-align: center\">">>CoronaVirus.rmd
echo "### Curva de casos positivos (acumulado) de COVID-19 en México">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=F}">>CoronaVirus.rmd
echo "library(tidyverse)">>CoronaVirus.rmd
echo "library(highcharter)">>CoronaVirus.rmd
echo "info <- data.frame(read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/general.csv\"))">>CoronaVirus.rmd
echo "info <- info %>% select(-c(X))">>CoronaVirus.rmd
echo "info\$Fecha <- as.Date(info\$Fecha, format=\"%d.%m.%Y\")">>CoronaVirus.rmd
echo "fecha.A <- info[nrow(info),3]">>CoronaVirus.rmd
echo "i <- 0">>CoronaVirus.rmd
echo "curva <- data.frame(Dia = as.Date(c(\"1992-08-08\")),">>CoronaVirus.rmd
echo "                        Total = 0)">>CoronaVirus.rmd
echo "while (fecha.A != \"2020-02-28\") {">>CoronaVirus.rmd
echo "  Dia <- info[nrow(info),3] - i">>CoronaVirus.rmd
echo "  fecha.A <- info[nrow(info),3] - i">>CoronaVirus.rmd
echo "  rango <- subset(info, Fecha == Dia)">>CoronaVirus.rmd
echo "  Total <- sum(rango\$n)">>CoronaVirus.rmd
echo "  temp <- data.frame(Dia,Total)">>CoronaVirus.rmd
echo "  curva <- rbind(curva,temp)">>CoronaVirus.rmd
echo "  i <- i+1">>CoronaVirus.rmd
echo "}">>CoronaVirus.rmd
echo "curva <- curva[-c(1),]">>CoronaVirus.rmd
echo "hchart(curva, \"line\", hcaes(x = Dia, y = Total)) %>%">>CoronaVirus.rmd
echo "  hc_add_theme(hc_theme_google())">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "</div>">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=F}">>CoronaVirus.rmd
echo " data <- read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/general.csv\")">>CoronaVirus.rmd
echo " ESTADOS <- c(\"Distrito Federal\",\"Aguascalientes\",\"Baja California\",">>CoronaVirus.rmd
echo "             \"Chiapas\",\"Chihuahua\",\"Coahuila\",\"Colima\",">>CoronaVirus.rmd
echo "             \"Durango\",\"Guanajuato\",\"Guerrero\",">>CoronaVirus.rmd
echo "             \"Jalisco\",\"México\",\"Nuevo León\",">>CoronaVirus.rmd
echo "             \"Oaxaca\",\"Puebla\", \"Querétaro\",">>CoronaVirus.rmd
echo "             \"Quintana Roo\",\"San Luis Potosí\",">>CoronaVirus.rmd
echo "             \"Sinaloa\",\"Sonora\",\"Tamaulipas\",\"Tabasco\",">>CoronaVirus.rmd
echo "             \"Veracruz\",\"Yucatán\",\"Hidalgo\",">>CoronaVirus.rmd
echo "		   \"Baja California Sur\",\"Zacatecas\", \"Nayarit\",">>CoronaVirus.rmd
echo "		   \"Michoacán\",\"Campeche\",\"Tlaxcala\")">>CoronaVirus.rmd
echo " lineaTemp <- function(est){">>CoronaVirus.rmd
echo "   df <- subset(data, Estado == est)">>CoronaVirus.rmd
echo "   df <- df %>% select(-c(X,Estado))">>CoronaVirus.rmd
echo "   df\$Fecha <- as.Date(as.character(df\$Fecha),format=\"%d.%m.%Y\")">>CoronaVirus.rmd
echo "   df.tmp <- xts(df\$n, order.by = df\$Fecha,frequency = 365)">>CoronaVirus.rmd
echo "   return(df.tmp)">>CoronaVirus.rmd
echo " }">>CoronaVirus.rmd
echo " linea.de.tiempo <- lineaTemp(ESTADOS[1])">>CoronaVirus.rmd
echo " i <- 1">>CoronaVirus.rmd
echo " while (i != length(ESTADOS)) {">>CoronaVirus.rmd
echo "   i <- i+1">>CoronaVirus.rmd
echo "   tmp <- lineaTemp(ESTADOS[i])">>CoronaVirus.rmd
echo "   linea.de.tiempo <- cbind(linea.de.tiempo,tmp)">>CoronaVirus.rmd
echo " }">>CoronaVirus.rmd
echo " rm(tmp)">>CoronaVirus.rmd
echo " dygraph(linea.de.tiempo,ylab=\"n\", ">>CoronaVirus.rmd
echo "         main=\"Linea de Tiempo por Estado\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"linea.de.tiempo\",label=\"Distrito Federal\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp\",label=\"Aguascalientes\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.1\",label=\"Baja California\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.2\",label=\"Chiapas\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.3\",label=\"Chiuahua\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.4\",label=\"Coahulia\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.5\",label=\"Colima\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.6\",label=\"Durango\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.7\",label=\"Guanajuato\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.8\",label=\"Guerrero\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.9\",label=\"Jalisco\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.10\",label=\"E. México\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.11\",label=\"Nuevo León\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.12\",label=\"Oaxaca\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.13\",label=\"Puebla\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.14\",label=\"Querétaro\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.15\",label=\"Quintana Roo\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.16\",label=\"San Luis Potosí\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.17\",label=\"Sinaloa\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.18\",label=\"Sonora\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.19\",label=\"Tamaulipas\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.20\",label=\"Tabasco\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.21\",label=\"Veracruz\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.22\",label=\"Yucatán\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.23\",label=\"Hidalgo\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.24\",label=\"Baja California Sur\") %>% ">>CoronaVirus.rmd
echo "  dySeries(\"tmp.25\",label=\"Zacatecas\") %>% ">>CoronaVirus.rmd
echo "  dySeries(\"tmp.26\",label=\"Nayarit\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.27\",label=\"Michoacán\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.28\",label=\"Campeche\") %>%">>CoronaVirus.rmd
echo "  dySeries(\"tmp.29\",label=\"Tlaxcala\") %>%">>CoronaVirus.rmd
echo "  dyRangeSelector()">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "<div style=\"text-align: center\">">>CoronaVirus.rmd
echo "### Aumento de casos del día "$(($1-1))"/04/2020 al "$1"/04/2020 por estado">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=F}">>CoronaVirus.rmd
echo "library(tidyverse)">>CoronaVirus.rmd
echo "library(highcharter)">>CoronaVirus.rmd
echo "info <- data.frame(read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/general.csv\"))">>CoronaVirus.rmd
echo "info <- info %>% select(-c(X))">>CoronaVirus.rmd
echo "info\$Fecha <- as.Date(info\$Fecha, format=\"%d.%m.%Y\")">>CoronaVirus.rmd
echo "i = 0">>CoronaVirus.rmd
echo "diferencia.dia <- data.frame(\"Estado\"=as.character(c(\"X\")),">>CoronaVirus.rmd
echo "                             \"Casos\" = 0)">>CoronaVirus.rmd
echo "while (i <= 31) {">>CoronaVirus.rmd
echo "  hoy <- info[(nrow(info)-i),2]">>CoronaVirus.rmd
echo "  ayer<- info[((nrow(info)-i)-32),2]">>CoronaVirus.rmd
echo "  Estado <- as.character(info[(nrow(info)-i),1])">>CoronaVirus.rmd
echo "  Casos <- hoy - ayer">>CoronaVirus.rmd
echo "  temp <- data.frame(Estado,Casos)">>CoronaVirus.rmd
echo "  diferencia.dia <- rbind(diferencia.dia,temp)">>CoronaVirus.rmd
echo "  i <- i +1">>CoronaVirus.rmd
echo "}">>CoronaVirus.rmd
echo "diferencia.dia <- diferencia.dia[-c(1),]">>CoronaVirus.rmd
echo "diferencia.dia <- diferencia.dia[order(diferencia.dia\$Casos, decreasing = TRUE), ]">>CoronaVirus.rmd
echo "hchart(diferencia.dia, \"column\", hcaes(x = Estado, y = Casos)) %>% ">>CoronaVirus.rmd
echo "  hc_add_theme(hc_theme_gridlight())">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo " ">>CoronaVirus.rmd
echo "### Gráfica por sexo de casos confirmados">>CoronaVirus.rmd
echo " ">>CoronaVirus.rmd
echo "\`\`\`{r echo=F, message=F}">>CoronaVirus.rmd
echo "library(tidyverse)">>CoronaVirus.rmd
echo "library(highcharter)">>CoronaVirus.rmd
echo "info <- data.frame(read.csv(\"/home/zorro/Documentos/Otros/COVID-19/covidmx/CSV/Abril/"$1".04.2020.base.limpia.csv\"))">>CoronaVirus.rmd
echo "temp1 <- data.frame(Sexo = \"Masculino\",">>CoronaVirus.rmd
echo "                 Total = subset(info, Sexo == \"Masculino\")%>% count())">>CoronaVirus.rmd
echo "temp2<- data.frame(Sexo = \"Femenino\",">>CoronaVirus.rmd
echo "                   Total = subset(info, Sexo == \"Femenino\")%>% count())">>CoronaVirus.rmd
echo "BD <- rbind(temp1,temp2)">>CoronaVirus.rmd
echo "hchart(BD , \"pie\", hcaes(x = Sexo, y = n)) %>%">>CoronaVirus.rmd
echo "  hc_add_theme(hc_theme_538())">>CoronaVirus.rmd
echo "\`\`\`">>CoronaVirus.rmd
echo "</div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"><b><a href=\"https://www.gob.mx/salud/documentos/coronavirus-covid-19-comunicado-tecnico-diario-238449\">Fuente General</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"><b><a href=\"https://gitlab.com/jucaoma/covidmx\">Nuestro GitLab(Archivos CSV, código fuente)</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Procesado con R v3.6.3 y RStudio v1.2.1335 </b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Autor: <a href=\"https://t.me/ZoRrO08\">ZoRrO</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/Ninjaburger\">Ninjaburger</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaboradora: <a href=\"https://t.me/Katjins\">Katjins</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/jucaoma\">Jucaoma</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/LaDataNo\">LaDataNo</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"> <b>Colaborador: <a href=\"https://t.me/petrohs\">petrohs</a></b></div>">>CoronaVirus.rmd
echo " <div style=\"text-align: right\"><b>Licencia: <a href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC-BY-SA</a></b></div>">>CoronaVirus.rmd
# Se borran algunos temporales
rm .tmp*.csv limpiar.base.r
# Se ejecuta RStudio con el archivo rmd recién creado
rstudio CoronaVirus.rmd
# Se modifica la estética final del html con sed
sed -E -i.bak '/./{H;$!d} ; x ; s/(<h3>Total para el día [[:digit:]]+\/[[:digit:]]+\/[[:digit:]]+:)<\/h3>\s*<pre><code>[^;]+;([[:digit:]]+)\s*Casos&quot;<\/code><\/pre>/\1 <b>\2<\/b> Casos<\/h3>/' CoronaVirus.html
sed -E -i.bak '/./{H;$!d} ; x ; s/(<h3>Total de confirmados para los últimos 14 días [[:digit:]]+\/[[:digit:]]+\/[[:digit:]]+:)<\/h3>\s*<pre><code>[^;]+;([[:digit:]]+)\s*Casos&quot;<\/code><\/pre>/\1 <b>\2<\/b> Casos<\/h3>/' CoronaVirus.html
# Se renombra el archivo html para que sea el mostrado en la pagina
mv CoronaVirus.html index.html
nano index.html
# Se realiza el push al repositorio por medio de la red Tor
#git add index.html CoronaVirus.rmd CSV/Abril/$1.04.2020* PDF/Abril/$1.04.2020.pdf
#git commit -m "Actualización diaria"
#torsocks git push
